package pl.pstatuch.vistair;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

import pl.pstatuch.vistair.beans.Aircraft;
import pl.pstatuch.vistair.daos.InMemoryAircraftsDao;

public class InMemoryAircraftsDaoTest {

	private final Aircraft VST100 = new Aircraft();
	private final Aircraft AIRONE = new Aircraft();
	
	InMemoryAircraftsDao dao;
	
	@Before
	public void before() {
		dao = new InMemoryAircraftsDao();
		VST100.setRegistrationNumber("VST100");
		VST100.setAirline("easyJet");
		VST100.setManufacturer("Boeing");
		VST100.setType("A380");	
	
		AIRONE.setRegistrationNumber("AIRONE");
	}
	
	@Test
	public void givenNewDaoWhenGetAllAircraftsThenEmptyCollection() {
		InMemoryAircraftsDao dao = new InMemoryAircraftsDao();
		assertTrue(dao.getAllAircrafts().isEmpty() ); 
	}

	@Test
	public void givenNewDaoWhenAddAircraftThenReturnTrueAndAircraftAdded() {
		assertTrue(dao.addAircraft(VST100));
		assertTrue(dao.getAllAircrafts().contains(VST100));
		assertEquals(1, dao.getAllAircrafts().size());
	}
	
	@Test
	public void givenSavedAircraftWhenAddAircraftThenReturnFalseAndUndateAircraft() {
		dao.addAircraft(VST100);
		String type = "787";
		VST100.setType(type);
		
		assertFalse(dao.addAircraft(VST100));
		Collection<Aircraft> aircrafts = dao.getAllAircrafts();
		assertEquals(1, aircrafts.size());
		assertTrue(aircrafts.contains(VST100));
		assertEquals(type, aircrafts.iterator().next().getType());
	}
	
	@Test
	public void givenDifferentAircraftsWhenAddAircraftThenPersistBoth() {
		dao.addAircraft(AIRONE);
		dao.addAircraft(VST100);
		
		Collection<Aircraft> aircrafts = dao.getAllAircrafts();
		assertEquals(2, aircrafts.size());
		assertTrue(aircrafts.contains(VST100));
		assertTrue(aircrafts.contains(AIRONE));		
	}
}