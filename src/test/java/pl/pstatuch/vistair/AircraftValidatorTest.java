package pl.pstatuch.vistair;

import static org.junit.Assert.*;

import java.util.Optional;

import javax.print.DocFlavor.STRING;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.pstatuch.vistair.beans.Aircraft;
import pl.pstatuch.vistair.services.AircraftValidator;

public class AircraftValidatorTest {

	private AircraftValidator validator;

	@Before
	public void before() {
		validator = new AircraftValidator();
	}

	@Test
	public void givenNullCheckThenReturnNullDescr() {
		Optional<String> result = validator.check(null);
		Assert.assertTrue(result.isPresent());
		Assert.assertEquals("Null", result.get());
	}

	@Test
	public void givenAircraftWithRegNumCheckThenReturnNoErrors() {
		Aircraft aircraft = new Aircraft();
		aircraft.setRegistrationNumber("TEST");
		Assert.assertFalse(validator.check(aircraft).isPresent());
	}
	
	@Test
	public void givenAircraftWithoutRegNumCheckThenReturnMissingReg() {
		Aircraft aircraft = new Aircraft();
		Optional<String> result = validator.check(aircraft);
		Assert.assertTrue(result.isPresent());
		Assert.assertEquals("Missing registration number", result.get());
	}
}
