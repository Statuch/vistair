package pl.pstatuch.vistair;

import static org.junit.Assert.assertTrue;
import java.util.Collections;
import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.stereotype.Service;

import pl.pstatuch.vistair.beans.Aircraft;
import pl.pstatuch.vistair.daos.AircraftsDao;
import pl.pstatuch.vistair.services.AircraftValidator;
import pl.pstatuch.vistair.services.SimpleAircraftsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class SimpleAircraftsServiceImplTest {

	@Rule
	public ExpectedException expected = ExpectedException.none();
	
	private final Aircraft VST100 = new Aircraft();
	
	{
		VST100.setRegistrationNumber("VST100");
	}

	@Mock
	public AircraftsDao dao;

	@Mock 
	public AircraftValidator validator;
	
	@InjectMocks
	SimpleAircraftsServiceImpl aircraftsService = new SimpleAircraftsServiceImpl();

	@Test
	public void whenAddAircraft_thenCallDao() {
		Mockito.when(dao.addAircraft(VST100)).thenReturn(true);
		Mockito.when(validator.check(VST100)).thenReturn(Optional.<String>empty());
		assertTrue(aircraftsService.addAircrasft(VST100));
		Mockito.verify(dao).addAircraft(VST100);
	}
	
	@Test
	public void givenInvalidAircraft_whenAddAircraft_thenThrowIllegalArgumentExc() {
		Mockito.when(dao.addAircraft(VST100)).thenReturn(true);
		Mockito.when(validator.check(Mockito.any(Aircraft.class))).thenReturn(Optional.<String>of("Null"));
		
		expected.expect(IllegalArgumentException.class);
		aircraftsService.addAircrasft(null);
	}


	@Test
	public void whenGetAllAircrafts_thenCallDao() {
		Mockito.when(dao.getAllAircrafts()).thenReturn(Collections.emptyList());
		assertTrue(aircraftsService.getAllAircrafts().isEmpty());
		Mockito.verify(dao).getAllAircrafts();
	}
}
