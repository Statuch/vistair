package pl.pstatuch.vistair;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VistairApplication {
	public static void main(String[] args) {
		SpringApplication.run(VistairApplication.class, args);
	}
}