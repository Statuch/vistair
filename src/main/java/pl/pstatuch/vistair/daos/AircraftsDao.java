package pl.pstatuch.vistair.daos;

import java.util.Collection;

import pl.pstatuch.vistair.beans.Aircraft;

public interface AircraftsDao {
	
	/**
	 * Return collection of {@link Aircraft}}
	 * @return
	 */
	Collection<Aircraft> getAllAircrafts();
	
	/**
	 * Adds new or replace existing aircraft
	 * @param aircraft
	 * @return true if new aircraft was added, false if registry contained aircraft with same registration number
	 */
	boolean addAircraft(Aircraft aircraft);
}
