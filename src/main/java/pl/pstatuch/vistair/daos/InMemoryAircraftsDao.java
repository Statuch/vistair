package pl.pstatuch.vistair.daos;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import pl.pstatuch.vistair.beans.Aircraft;

@Repository
@Qualifier("inMemoryAircrafts")
public class InMemoryAircraftsDao implements AircraftsDao{

	private final Object SYNCHRONIZE = new Object();
	private Map<String, Aircraft> aircrafts = new HashMap<>();
	
	@Override
	public Collection<Aircraft> getAllAircrafts() {
		return aircrafts.values();
	}

	@Override
	public boolean addAircraft(Aircraft aircraft) {
		synchronized (SYNCHRONIZE) {
			return aircrafts.put(aircraft.getRegistrationNumber(), aircraft) == null;
		}
	}
}
