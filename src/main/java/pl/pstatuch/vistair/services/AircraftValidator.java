package pl.pstatuch.vistair.services;

import java.util.Optional;

import org.springframework.stereotype.Component;

import pl.pstatuch.vistair.beans.Aircraft;

@Component
public class AircraftValidator {

	/**
	 * check if all field are present.
	 * 
	 * @param aircraft
	 * @return descriptin of problems with object
	 */
	public Optional<String> check(Aircraft aircraft) {
		if (aircraft == null) {
			return Optional.of("Null");
		}
		else if (aircraft.getRegistrationNumber() == null || aircraft.getRegistrationNumber().isEmpty()) {
			return Optional.of("Missing registration number");
		} else {
			return Optional.empty();
		}

	}
}
