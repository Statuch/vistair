package pl.pstatuch.vistair.services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.pstatuch.vistair.beans.Aircraft;

@RestController
@RequestMapping("/aircrafts")
public class AircraftsController {

	@Autowired
	private AircraftsService aircraftsService;

	@RequestMapping(method = RequestMethod.GET)
	public Collection<Aircraft> getAircrafts() {
		return aircraftsService.getAllAircrafts();
	}

	@RequestMapping(method = RequestMethod.POST, consumes="application/json")
	public boolean addAircraft(@RequestBody Aircraft aircraft) {
		return aircraftsService.addAircrasft(aircraft);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public String wrongData( IllegalArgumentException exc, ServletServerHttpResponse response) {
		response.setStatusCode(HttpStatus.BAD_REQUEST);
		return "Invalid aircraft data : "+exc.getMessage();
	}
}
