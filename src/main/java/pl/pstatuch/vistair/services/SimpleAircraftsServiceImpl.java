package pl.pstatuch.vistair.services;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import pl.pstatuch.vistair.beans.Aircraft;
import pl.pstatuch.vistair.daos.AircraftsDao;

@Service
public class SimpleAircraftsServiceImpl implements AircraftsService{
	
	@Autowired
	@Qualifier("inMemoryAircrafts")
	private AircraftsDao dao;

	@Autowired
	private  AircraftValidator validator;
	
	@Override
	public boolean addAircrasft(Aircraft aircraft) {
		Optional<String> problem = validator.check(aircraft);
		if(problem.isPresent()) {
			throw new IllegalArgumentException(problem.get());
		}
		
		return dao.addAircraft(aircraft);
	}

	@Override
	public Collection<Aircraft> getAllAircrafts() {
		return dao.getAllAircrafts();
	}
}
