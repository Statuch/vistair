package pl.pstatuch.vistair.services;

import java.util.Collection;

import pl.pstatuch.vistair.beans.Aircraft;

public interface AircraftsService {
	
	public boolean addAircrasft(Aircraft aircraft);
	
	public Collection<Aircraft> getAllAircrafts();
}
